#!/bin/bash

# Wechsel in das Verzeichnis, wo dieses Skript gespeichert ist
THIS_SCRIPTS_PATH=$( cd "$(dirname "${BASH_SOURCE}")" ; pwd -P )
cd $THIS_SCRIPTS_PATH
BEGIN=$(date +%s)

################################################################################
# HIER konfigurieren

# Lege fest wo sich der Generierte Code befindet
INPUT_DIR=./CIM/16v29a_Extension/unmodified
OUTPUT_DIR=$INPUT_DIR/../refactored
PATCHES_DIR=./CIM/16v29a_Extension/patches


CIMINCTOOL=../CIM-Include-Tool/build/CIM-Include-Tool
CIMREFACTORER=../CIMRefactorer/build/CIMRefactorer
CIMUNMARSHALLINGGENERATOR=../CIMUnmarshallingGenerator/build/CIMUnmarshallingGenerator
WHITELIST=../CIMUnmarshallingGenerator/resource/whitelist.txt
DUMMY_STL=../CIMRefactorer/resource/DummySTL
LLVM_INSTALL=/usr/lib/llvm-3.8
################################################################################

# Detect OSTYPE for sed
case "$OSTYPE" in
	darwin*) SYSTEM=BSD ;;
	linux*)  SYSTEM=GNU ;;
	bsd*)    SYSTEM=BSD ;;
	*)       echo "unknown: $OSTYPE"
	         exit;;
esac

# Teste ob die Tools existieren
if ! [ -e $CIMINCTOOL ]; then
	echo "CIMINCTOOL does not exist"
	exit
fi

if ! [ -e $CIMREFACTORER ];then
	echo "CIMREFACTORER does not exist"
	exit
fi

if ! [ -e $WHITELIST ];then
	echo "WHITELIST does not exist"
	exit
fi

if ! [ -e $LLVM_INSTALL/bin/llvm-config ];then
	echo "llvm-config does not exist"
	exit
fi

if ! [ -e $INPUT_DIR/IEC61970/IEC61970CIMVersion.h ];then
	echo "Code from EA does not exist"
	exit
fi

if [ -d $OUTPUT_DIR ];then
	echo "Output directory already exists"
	exit
fi

# Detect LLVM_VERSION
LLVM_VERSION=$($LLVM_INSTALL/bin/llvm-config --version)

# Pause function
stop_if()
{
	if [[ $1 -ne 0 ]]
	then
		echo "Last command returned $1, try to fix the problem"
		read -n1 -r -p "Press any key to continue..." key
	fi
}

# Copy code from input directory to output directory
cp -R $INPUT_DIR $INPUT_DIR/../tmp
mv $INPUT_DIR/../tmp $OUTPUT_DIR
CODE_PREFIX=$OUTPUT_DIR

# Ändere in IEC61970CIMVersion.cpp
# - den Aufruf des Date Konstruktors
# - die Zuweisung des Versionsstrings
if [ x$SYSTEM == x"BSD" ]; then
	sed -E -i '' 's/IEC61970CIMVersion::date = ([0-9-]*);/IEC61970CIMVersion::date = IEC61970::Base::Domain::Date("\1");/' $CODE_PREFIX/IEC61970/IEC61970CIMVersion.cpp
	sed -E -i '' 's/IEC61970CIMVersion::version = ([a-zA-Z0-9]*);/IEC61970CIMVersion::version = "\1";/' $CODE_PREFIX/IEC61970/IEC61970CIMVersion.cpp
else
	sed -r -i 's/IEC61970CIMVersion::date = ([0-9-]*);/IEC61970CIMVersion::date = IEC61970::Base::Domain::Date("\1");/' $CODE_PREFIX/IEC61970/IEC61970CIMVersion.cpp
	sed -r -i 's/IEC61970CIMVersion::version = ([a-zA-Z0-9]*);/IEC61970CIMVersion::version = "\1";/' $CODE_PREFIX/IEC61970/IEC61970CIMVersion.cpp
fi

# Wende die Patches an
echo "Patching files in IEC61970..."
for patch in $PATCHES_DIR/*.patch;
do
	patch -d  "$CODE_PREFIX" -p1 --silent < "$patch"
	stop_if $?
done

if [ -e $INPUT_DIR/IEC61968 ]; then
	echo "Patching files in IEC61968..."
	for patch in $PATCHES_DIR/P_IEC61968/*.patch;
	do
		patch -d "$CODE_PREFIX" -p1 --silent < "$patch"
		stop_if $?
	done

	if [ x$SYSTEM == x"BSD" ]; then
		sed -E -i '' 's/IEC61968CIMVersion::date = ([0-9-]*);/IEC61968CIMVersion::date = IEC61970::Base::Domain::Date("\1");/' $CODE_PREFIX/IEC61968/IEC61968CIMVersion.cpp
		sed -E -i '' 's/IEC61968CIMVersion::version = ([a-zA-Z0-9]*);/IEC61970CIMVersion::version = "\1";/' $CODE_PREFIX/IEC61968/IEC61968CIMVersion.cpp
	else
		sed -r -i 's/IEC61968CIMVersion::date = ([0-9-]*);/IEC61968CIMVersion::date = IEC61970::Base::Domain::Date("\1");/' $CODE_PREFIX/IEC61968/IEC61968CIMVersion.cpp
		sed -r -i 's/IEC61968CIMVersion::version = ([a-zA-Z0-9]*);/IEC61968CIMVersion::version = "\1";/' $CODE_PREFIX/IEC61968/IEC61968CIMVersion.cpp
	fi
fi

#echo "Reached end of script"
#exit

# Delete files
rm $CODE_PREFIX/IEC61970/Base/Domain/Float.cpp
rm $CODE_PREFIX/IEC61970/Base/Domain/Integer.cpp
rm $CODE_PREFIX/IEC61970/Base/Domain/String.cpp

# Führe CodeRefactorer aus
echo "Running CIM-Include-Tool..."
$CIMINCTOOL $CODE_PREFIX &> /dev/null

# Finde alle Dateien in $CODE_PREFIX und speichere sie in einem Array
array=()
while IFS=  read -r -d $'\0'; do
		array+=("$REPLY")
done < <(find $CODE_PREFIX -type f \( -name "*.h" -or -name "*.cpp" \) -print0)

# Sorge dafür, dass alle Dateien mit einem new line enden (POSIX-Standard)
echo "Make files POSIX conform..."
for file in "${array[@]}";
do
	if [ x$SYSTEM == x"BSD" ]; then
		sed -i '' -e '$a\' $file
	else
		sed -i -e '$a\' $file
	fi
done

# Führe das clang tool aus
echo "Running CIMRefactorer..."
echo "Go grab a cup of coffee, this will take a while."
$CIMREFACTORER $CODE_PREFIX $WHITELIST
stop_if $?

cp $THIS_SCRIPTS_PATH/Patches/BaseClass.h $CODE_PREFIX

# Führe interaktiv clang-check aus
echo "Running clang-check"
retry=true
counter=1
num_of_files=${#array[@]}
for file in "${array[@]}";
do
	echo "[$counter/$num_of_files]" $file
	((counter++))
	retry=true
	while [ $retry == "true" ];
	do
		$LLVM_INSTALL/bin/clang-check $file -- -I$CODE_PREFIX -I$DUMMY_STL -I$LLVM_INSTALL/lib/clang/$LLVM_VERSION/include -x c++ -std=c++11 &> /dev/null
		if [[ $? != "0" ]]; then
			echo Fallback mode
			$LLVM_INSTALL/bin/clang-check $file -- -I$CODE_PREFIX -I$LLVM_INSTALL/lib/clang/$LLVM_VERSION/include -x c++ -std=c++11
			if [[ $? != "0" ]]; then
				echo "You can now fix the error"
				read -p "Yes, I fixed it and want to retry. No, continue anyway. [yn]" yn
				case $yn in
					[Yy]* ) retry=true;;
					[Nn]* ) retry=false;;
					* ) retry=true;;
				esac
			else retry=false
			fi
		else retry=false
		fi
	done
done


# Delete old files of Primitive Datatypes
rm $CODE_PREFIX/IEC61970/Base/Domain/Float.h
rm $CODE_PREFIX/IEC61970/Base/Domain/Integer.h
rm $CODE_PREFIX/IEC61970/Base/Domain/Duration.h
rm $CODE_PREFIX/IEC61970/Base/Domain/Duration.cpp
rm $CODE_PREFIX/IEC61970/Base/Domain/Date.h
rm $CODE_PREFIX/IEC61970/Base/Domain/Date.cpp
rm $CODE_PREFIX/IEC61970/Base/Domain/DateTime.h
rm $CODE_PREFIX/IEC61970/Base/Domain/DateTime.cpp
rm $CODE_PREFIX/IEC61970/Base/Domain/MonthDay.h
rm $CODE_PREFIX/IEC61970/Base/Domain/MonthDay.cpp

#Kopiere alle primitiven Datentypen in den Ordner

cp -R $THIS_SCRIPTS_PATH/PrimitiveDatatypes/. $CODE_PREFIX/IEC61970/Base/Domain/


# Generiere den IEC61970 Header
cd $CODE_PREFIX
echo "" > Folders.hpp
find ./IEC61970 -name "*.h" > IEC61970.hpp.tmp
find ./*/ -name "*.h" -not -path "./IEC*" >> IEC61970.hpp.tmp
if [ x$SYSTEM == x"BSD" ]; then
	sed -i '' -E 's/\.\/(.*)$/#include "\1"/' IEC61970.hpp.tmp
else
	sed -i -r 's/\.\/(.*)$/#include "\1"/' IEC61970.hpp.tmp
fi
mv IEC61970.hpp.tmp IEC61970.hpp
echo "#include \"IEC61970.hpp\"" >> Folders.hpp

if [ -e ./IEC61968 ]; then
	find ./IEC61968 -name "*.h" > IEC61968.hpp.tmp
	if [ x$SYSTEM == x"BSD" ]; then
		sed -i '' -E 's/\.\/(.*)$/#include "\1"/' IEC61968.hpp.tmp
	else
		sed -i -r 's/\.\/(.*)$/#include "\1"/' IEC61968.hpp.tmp
	fi
	mv IEC61968.hpp.tmp IEC61968.hpp
	echo "#include \"IEC61968.hpp\"" >> Folders.hpp
fi

if [ -e ./IEC61970Extension ]; then
	find ./IEC61970Extension -name "*.h" > IEC61970Extension.hpp.tmp
	if [ x$SYSTEM == x"BSD" ]; then
		sed -i '' -E 's/\.\/(.*)$/#include "\1"/' IEC61970Extension.hpp.tmp
	else
		sed -i -r 's/\.\/(.*)$/#include "\1"/' IEC61970Extension.hpp.tmp
	fi
	mv IEC61970Extension.hpp.tmp IEC61970Extension.hpp
	echo "#include \"IEC61970Extension.hpp\"" >> Folders.hpp
fi


cd $THIS_SCRIPTS_PATH

# Generiere den Unmarshaller
echo "Generate the unmarshaller"
$CIMUNMARSHALLINGGENERATOR $CODE_PREFIX
mv *.cpp $CODE_PREFIX
mv *.hpp $CODE_PREFIX

END=$(date +%s)
DIFF=$(($END - $BEGIN))
SECS=$(($DIFF % 60))
echo "This script took $SECS seconds to execute"
