# README

## HOWTO for Xubuntu 17.04
Edit `/etc/apt/sources.list`:<br>
Replace all `de.archive.` and `security.` by `old-releases.`

### Preparations
```bash
sudo apt update
sudo apt upgrade
sudo reboot

sudo apt install build-essential git cmake
sudo apt install libboost-all-dev

sudo apt install clang-3.8 clang-3.8-dev
sudo apt install libtclap-dev zlib1g-dev libctemplate-dev

mkdir gits
cd gits
git clone https://git.rwth-aachen.de/acs/core/cim/cimpp/CIMUnmarshallingGenerator.git
git clone https://git.rwth-aachen.de/acs/core/cim/cimpp/CIMRefactorer.git
git clone https://git.rwth-aachen.de/acs/core/cim/cimpp/Refactoring-Script.git
git clone https://git.rwth-aachen.de/acs/core/cim/cimpp/CIM-Include-Tool.git
```

### CIM-Include-Tool
```bash
cd CIM-Include-Tool/
mkdir build
cd build
cmake ..
make -j 8
```

### CIMRefactorer
```bash
cd ~/gits/CIMRefactorer
```
Adapt the Makefile:
```make
...
CXX = clang++-3.8

# Set LLVM installation directory
LLVM_INSTALL_DIR=/usr/lib/llvm-3.8
BOOST_INSTALL_DIR=/usr/lib/x86_64-linux-gnu

# Set DUMMY_STL
DUMMY_STL = /home/acs-admin/gits/CIMRefactorer/resource/DummySTL

# STOP! All configuration done!

# Include directories
INCDIR =\
                -I$(LLVM_INSTALL_DIR)/include \
                -I$(BOOST_INSTALL_DIR)/include

# Libraries directories
LIBDIR =\
                -L$(LLVM_INSTALL_DIR)/lib \
                -L$(BOOST_INSTALL_DIR)
...
```
And build it:
```bash
make -j 8
```

### CIMUnmarshallingGenerator
```bash
cd ~/gits/CIMUnmarshallingGenerator
```
Adapt the Makefile:
```make
...
CXX = clang++-3.8

# Set LLVM installation directory
LLVM_INSTALL_DIR=/usr/lib/llvm-3.8
BOOST_INSTALL_DIR=/usr/lib/x86_64-linux-gnu
CTEMPLATE_INSTALL_DIR=/usr

# Set DUMMY_STL
DUMMY_STL = /home/acs-admin/gits/CIMRefactorer/resource/DummySTL

# STOP! All configuration done!

# Include directories
INCDIR =\
                -I$(LLVM_INSTALL_DIR)/include \
                -I$(BOOST_INSTALL_DIR)/include \
                -I$(CTEMPLATE_INSTALL_DIR)/include

# Libraries directories
LIBDIR =\
                -L$(LLVM_INSTALL_DIR)/lib \
                -L$(BOOST_INSTALL_DIR) \
                -L$(CTEMPLATE_INSTALL_DIR)/lib
...
```
And built it:
```bash
make -j 8
```

### Refactoring-Script
```bash
cd ~/gits/Refactoring-Script
```
Adapt `.gitmodules`if needed.
```bash
git submodule update --init
rm -R CIM/17v07/refactored
```
Adapt `refactoring.sh`:
```bash
...
INPUT_DIR=./CIM/17v07/unmodified
OUTPUT_DIR=$INPUT_DIR/../refactored
PATCHES_DIR=./CIM/17v07/patches


CIMINCTOOL=../CIM-Include-Tool/build/CIM-Include-Tool
CIMREFACTORER=../CIMRefactorer/build/CIMRefactorer
CIMUNMARSHALLINGGENERATOR=../CIMUnmarshallingGenerator/build/CIMUnmarshallingGenerator
WHITELIST=../CIMUnmarshallingGenerator/resource/whitelist.txt
DUMMY_STL=../CIMRefactorer/resource/DummySTL
LLVM_INSTALL=/usr/lib/llvm-3.8
...
```
And run the script:
```bash
./refactoring.sh
```
